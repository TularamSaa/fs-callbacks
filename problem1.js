const fs = require("fs");
const path = require("path");

const deleteFiles = (dirName) => {
  fs.readdir(dirName, (err, files) => {
    if (err) {
      console.log(err);
    } else {
      // console.log(files);
      return files.map((file) => {
        const filePath = path.join(dirName, file);
        // console.log(filePath);
        fs.unlink(filePath, (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log(`Successfully deleted "${file}" `);
          }
        });
      });
    }
  });
};

const createJsonFiles = (dirPath, dirName) => {
  const file1 = () => {
    const file1Name = "file1.json";
    const file1Path = path.join(dirPath, file1Name);
    fs.writeFile(file1Path, JSON.stringify({ key: "value" }), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`file1.json created!`);
        file2();
      }
    });
  };
  const file2 = () => {
    const file2Name = "file2.json";
    const file2Path = path.join(dirPath, file2Name);
    fs.writeFile(file2Path, JSON.stringify({ key: "value" }), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`file2.json created!`);
        file3();
      }
    });
  };
  const file3 = () => {
    const file3Name = "file3.json";
    const file3Path = path.join(dirPath, file3Name);
    fs.writeFile(file3Path, JSON.stringify({ key: "value" }), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`file3.json created!`);
        file4();
      }
    });
  };
  const file4 = () => {
    const file4Name = "file4.json";
    const file4Path = path.join(dirPath, file4Name);
    fs.writeFile(file4Path, JSON.stringify({ key: "value" }), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`file4.json created!`);
        deleteFiles(dirName);
      }
    });
  };
  file1();
  //
};

const createDir = (dirName) => {
  const dirPath = path.join(__dirname, dirName);
  //   console.log(dirPath);

  fs.mkdir(dirPath, { recursive: true }, (err) => {
    if (err) {
      console.log(err);
    } else {
      createJsonFiles(dirPath, dirName);
      console.log(`Successfully created  "${dirName}" directory!`);
    }
  });
};

const problem1Function = (dirName) => {
  createDir(dirName);
};

module.exports = problem1Function;
