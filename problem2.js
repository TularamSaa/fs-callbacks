const fs = require("fs");
const path = require("path");

const storeFileName = (fileName) => {
  const fileToStoreIn = "filenames.txt";
  const filePath = path.join(__dirname, fileToStoreIn);
  fs.appendFile(filePath, fileName, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Filename ${fileName} stored in ${fileToStoreIn}`);
    }
  });
};
const deleteContentOfFileNames = (data) => {
  let individualFileName = data.split(".txt");
  //   console.log(individualFileName);
  individualFileName.pop();
  //   console.log(individualFileName);
  let arr = [];
  individualFileName.map((fileName) => {
    // console.log(fileName + ".txt");
    arr.push(path.join(__dirname, fileName + ".txt"));
  });

  for (let eachPath of arr) {
    fs.unlink(eachPath, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`${eachPath} successfully deleted`);
      }
    });
  }
};
const readContentOfFilenames = (fileName = "filenames.txt") => {
  filePath = path.join(__dirname, fileName);
  fs.readFile(filePath, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`successfully read ${fileName}`);
      deleteContentOfFileNames(data);
    }
  });
};
const sortLowerCaseData = (data) => {
  console.log(typeof data);
  let sortedData = data.split("\n").sort().join("\n");
  let fileName = "sortedData.txt";
  let filePath = path.join(__dirname, fileName);
  fs.writeFile(filePath, sortedData, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Successfully sorted our data");
      storeFileName(fileName);
      readContentOfFilenames();
    }
  });
};

const readSplitData = (fileName) => {
  const filePath = path.join(__dirname, fileName);
  fs.readFile(filePath, "utf-8", (err, data) => {
    //
    // console.log(data);
    if (err) {
      console.log(err);
    } else {
      sortLowerCaseData(data);
    }
  });
};

const dataToLowerCase = (data) => {
  const lowerCaseData = data.toLowerCase(data);
  //   console.log(lowerCaseData);
  const splitData = lowerCaseData.split(". ");
  //   console.log(splitData);
  const sentenceInStringformat = splitData.join("\n");
  //   console.log(sentenceInStringformat);
  const fileName = "lowerCaseData.txt";
  const filePath = path.join(__dirname, fileName);
  fs.writeFile(filePath, sentenceInStringformat, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Lowercase  data is successfully converted into sentences.");
      readSplitData(fileName);
      storeFileName(fileName);
    }
  });
};

const readLipsumFile = (fileName) => {
  const filePath = path.join(__dirname, fileName);
  fs.readFile(filePath, "utf-8", (err, data) => {
    // console.log(data);
    if (err) {
      console.log(err);
    } else {
      console.log(`data from ${fileName} read!`);
      dataToLowerCase(data);
    }
  });
};

const dataToUpperCase = (data) => {
  const upperCaseData = data.toUpperCase();
  // console.log(upperCaseData);
  const fileName = "lipsumUpperCaseData.txt";
  const filePath = path.join(__dirname, fileName);
  fs.writeFile(filePath, upperCaseData, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log(`Successfully converted data from lipsum.txt to uppercase.`);
      storeFileName(fileName);
      readLipsumFile(fileName);
    }
  });
};

const readLipsumTxtFile = (fileName) => {
  const filePath = path.join(__dirname, fileName);
  fs.readFile(filePath, "utf-8", (err, data) => {
    // console.log(data);
    if (err) {
      console.log(err);
    } else {
      console.log(`We successfully read lipsum.txt file.`);
      dataToUpperCase(data);
    }
  });
};

const problem2Function = (fileName) => {
  readLipsumTxtFile(fileName);
};

module.exports = problem2Function;
